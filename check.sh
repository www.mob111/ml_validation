flake8 ml_validation
mypy ml_validation
isort ml_validation --diff --check-only

nbqa flake8 mit_bih.ipynb ptbxl.ipynb
nbqa mypy mit_bih.ipynb ptbxl.ipynb
nbqa isort mit_bih.ipynb ptbxl.ipynb --diff --check-only
