import ast
from datetime import datetime
from pathlib import Path

import numpy as np
import numpy.typing as npt
import pandas as pd

from .metrics import get_metrics
from .predict import Function, XType, batch_predict
from .report import Report


class Experiment:
    YType = npt.NDArray[np.bool_]

    def __init__(self, authors: list[str], path: Path, task_type: str, test_fold: int) -> None:
        self._start = datetime.now().astimezone()
        self._authors = authors

        X = np.load(path / "ptb_xl.npy").astype(np.float32) / 1000
        meta = Experiment._load_metadata(path)
        assert len(X) == len(meta)

        self._X_train, self._meta_train, self._X_test, self._meta_test = Experiment._split(X, meta, test_fold)

        scp = pd.read_csv(path / "scp_statements.csv", index_col=0)
        self._classes = Experiment._get_classes(scp, task_type)

        scp_to_class = self._read_scp_to_class(scp, task_type)
        self._Y_train = self._get_classes_from_meta(self._meta_train, scp_to_class)
        self._Y_test = self._get_classes_from_meta(self._meta_test, scp_to_class)

    def get_data(self) -> tuple[XType, YType]:
        return self._X_train, self._Y_train

    def get_meta(self) -> pd.DataFrame:
        return self._meta_train

    def validate(self, func: Function, batch_size: int = 1) -> Report:
        classes = self._classes
        if len(classes) == 1:
            classes = [classes[0], "NOT_" + classes[0]]
        y_pred = batch_predict(func, self._X_test, len(self._classes), batch_size)
        table, matrices = get_metrics(self._Y_test, y_pred, classes)
        return Report(
            version=(0, 0, 1),
            start=self._start,
            end=datetime.now().astimezone(),
            authors=self._authors,
            table=table,
            matrices=matrices
        )

    @property
    def classes(self) -> list[str]:
        return self._classes

    @staticmethod
    def _get_classes(scp: pd.DataFrame, task_type: str) -> list[str]:
        if task_type == "binary":
            return ["NORM"]
        elif task_type == "superclasses":
            return ["NORM", "MI", "STTC", "CD", "HYP"]
        elif task_type == "all":
            return scp.index.to_list()
        else:
            raise RuntimeError(f"Wrong type of task: {task_type}")

    def _read_scp_to_class(self, scp: pd.DataFrame, task_type: str) -> dict[str, int]:
        if task_type == "binary":
            return {"NORM": 0}
        elif task_type == "superclasses":
            return self._read_scp_to_class_superclasses(scp)
        elif task_type == "all":
            return {c: i for i, c in enumerate(self._classes)}
        else:
            raise RuntimeError(f"Wrong type of task: {task_type}")

    def _read_scp_to_class_superclasses(self, scp: pd.DataFrame) -> dict[str, int]:
        scp = scp[scp.diagnostic == 1]
        scp_to_class = {}
        for d, c in zip(scp.index, scp.diagnostic_class):
            scp_to_class[d] = self._classes.index(c)
        return scp_to_class

    def _get_classes_from_meta(self, meta: pd.DataFrame, scp_to_class: dict[str, int]) -> YType:
        Y = np.zeros((len(meta), len(self._classes)), dtype=np.bool_)
        for i, codes in enumerate(meta.scp_codes):
            for code in codes:
                if code in scp_to_class:
                    Y[i, scp_to_class[code]] = True
        return Y

    @staticmethod
    def _load_metadata(path: Path) -> pd.DataFrame:
        meta = pd.read_csv(path / "ptbxl_database.csv", index_col="ecg_id")
        meta.scp_codes = meta.scp_codes.apply(lambda x: ast.literal_eval(x))
        return meta

    @staticmethod
    def _split(X: XType, meta: pd.DataFrame, test_fold: int) -> tuple[XType, pd.DataFrame, XType, pd.DataFrame]:
        X_train = X[np.where(meta.strat_fold != test_fold)]
        meta_train = meta[meta.strat_fold != test_fold]
        X_test = X[np.where(meta.strat_fold == test_fold)]
        meta_test = meta[meta.strat_fold == test_fold]
        return X_train, meta_train, X_test, meta_test


def start_experiment(
        authors: str | list[str],
        path_dir: Path | str = "datasets",
        task_type: str = "superclasses",
        test_fold: int = 10) -> Experiment:
    if isinstance(authors, str):
        authors = [authors]
    if isinstance(path_dir, str):
        path_dir = Path(path_dir)
    return Experiment(authors, path_dir, task_type, test_fold)
