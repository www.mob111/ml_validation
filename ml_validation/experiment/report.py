from __future__ import annotations

import json
from dataclasses import dataclass
from datetime import datetime
from pathlib import Path
from typing import Callable

import numpy as np
import numpy.typing as npt
import pandas as pd
from tabulate import tabulate


@dataclass
class Report:
    version: tuple[int, int, int]
    start: datetime
    end: datetime
    authors: list[str]

    table: pd.DataFrame
    matrices: dict[str, npt.NDArray[np.int64]]

    def __post_init__(self) -> None:
        assert self.authors

    def dump(self, path: Path | str) -> None:
        obj = {
            "version": self.version,
            "start": self.start.isoformat(),
            "end": self.end.isoformat(),
            "autors": self.authors,
            "table": self.table.to_dict(),
            "matrices": {name: matrix.tolist() for name, matrix in self.matrices.items()}
        }
        with open(path, "w") as f:
            json.dump(obj, f, indent=4)

    @staticmethod
    def load(path: Path | str) -> Report:
        with open(path, "r") as f:
            obj = json.load(f)
        return Report(
            version=obj["version"],
            start=datetime.fromisoformat(obj["start"]),
            end=datetime.fromisoformat(obj["end"]),
            authors=obj["autors"],
            table=pd.DataFrame(obj["table"]),
            matrices={name: np.array(matrix) for name, matrix in obj["matrices"].items()}
        )

    @staticmethod
    def _date_to_string(d: datetime) -> str:
        d_str = d.strftime("%c")
        if (tz := d.tzname()) is not None:
            d_str += f" {tz}"
        return d_str

    @staticmethod
    def _matrix_to_string(m: npt.NDArray[np.int64]) -> str:
        return pd.DataFrame(m).to_string(header=False, index=False)

    def _matrices_to_str(self) -> str:
        class_to_matrix = {c: [Report._matrix_to_string(m)] for c, m in self.matrices.items()}
        df = pd.DataFrame(class_to_matrix)
        num_columns = 5

        res = ""
        for begin in range(0, len(df.columns), num_columns):
            columns = df.columns[begin:begin + num_columns]
            matrices_str = tabulate(df[columns], tablefmt="grid", headers="keys", showindex=False)
            if begin:
                res += "\n"
            res += f"{matrices_str}\n"
        return res

    def print_matrices(self) -> None:
        print(self._matrices_to_str())

    def __str__(self) -> str:
        b: Callable[[str], str] = lambda x: "\033[1m" + x + "\033[0m"

        version = ".".join(str(x) for x in self.version)
        res = f"{b('Версия')}: {version}\n"
        res += f"{b('Начало')}: {Report._date_to_string(self.start)}\n"
        res += f"{b('Конец')}: {Report._date_to_string(self.end)}\n"

        if len(self.authors) == 1:
            res += f"{b('Автор')}: {self.authors[0]}\n"
        else:
            autors = ", ".join(self.authors)
            res += f"{b('Авторы')}: {autors}\n"

        res += f"\n{b('Метрики')}:\n"
        res += self.table.to_string(line_width=80, float_format=lambda x: str(round(x, 6)))

        res += f"\n\n{b('Матрицы рассогласования')}:\n\n"
        res += self._matrices_to_str()
        return res
