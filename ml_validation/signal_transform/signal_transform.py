from abc import abstractmethod
import numpy as np

from tqdm import tqdm
from scipy.signal import butter, lfilter, iirnotch


class SignalTransformer():
    def __init__(self, config={}):
        self.config = config

    def butter_bandpass(self, lowcut, highcut, fs, order=5):
        nyq = 0.5 * fs
        low = lowcut / nyq
        high = highcut / nyq
        b, a = butter(order, [low, high], btype='band')
        return b, a

    def butter_bandpass_filter(self, data, lowcut, highcut, fs, order=5):
        if self.config.get("butter_bandpass_filter", False):
            b, a = self.butter_bandpass(lowcut, highcut, fs, order=order)
            data = lfilter(b, a, data, axis=-1) # axis -1 assumes time-series data is in the last dimension
        return data

    def notch_filter(self, data, f0, fs, Q=30):
        if self.config.get("notch_filter", False):
            b, a = iirnotch(f0 / (0.5 * fs), Q)
            data = lfilter(b, a, data)
        return data

    def apply(self, X):
        fs = self.config.get('fs', 1000.0)
        lowcut = self.config.get('lowcut', 0.5)
        highcut = self.config.get('highcut', 100.0)

        X_new = np.array(
            [self.butter_bandpass_filter(
                self.notch_filter(signal, 50, fs), lowcut, highcut, fs, 4) 
                for signal in tqdm(X)], dtype='float32')

        return X_new
